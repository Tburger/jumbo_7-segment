Project Folder
================
* Hardware — contains the KiCAD Project, Schematics & Layout and Project Libraries
* Firmware — contains any software developed for the Hardware
* Docs — contains any data sheets or documentations
* Production — contains the gerber files, BOM or anything required by the fabrication houses
* Simulation — contains any simulation files and generated results
* CAD — contains the 3D models and mechanical designs for enclosures or support


By utilizing git you better manage your project history:

1. Accurate history for development process
2. Real Time collaboration with team
3. Improvise rapidly or develop multiple product variations in parallel using branches
4. Central Repositories for reusable components i.e. libraries or template boards.
