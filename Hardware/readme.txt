The KiCAD project should be initialized in that subfolder 'Hardware'.
The name of the project must be named after the hardware you need to develop.
Upon creating a new project, KiCAD creates several different files throughout
the development of the board, many of which are temporary files and unimportant.
Refer to the official KiCAD documentation to more detail.

These files that your KiCAD project really cares about are:

Project Manager File:
*.pro — a small file containing the component library lists for the current
project along with a few more parameters.


Schematic Files (eeschema):
*.sch — the schematic files of the project*_cache.lib — this is a local copy of
all the symbols used in the project. It contains each and every component used
in the schematic and it is very important to view schematic properly.


PCB Layout (pcbnew):
*.kicad_pcb — the pcb layout for the board design. It used to be a .brd file
for the older versions of KiCAD which has been deprecated since version 4.


Special File:
*.cpm — used for component symbol association in schematic file with the
relevant footprints in the pcb_layout. It is used to track changes from eeschema
(the schematics tool) to pcbnew (PCB Layout tool) or vice versa.


Any other files in the project folder are temporary files which should be
ignored by the version control system.


