Project Libraries:

A very convenient way of managing libraries is to gather all of them in a
subfolder named “Libraries” located in the KiCAD project folder. If you are more
comfortable with using git version control, you can go ahead and initiate a
submodule for global library management.
See https://medium.com/inventhub/open-source-hardware-the-licenses-a244733e6cb7

Simply copying and pasting libraries in the 'Libraries' folder also works well.
The library files are:


For Schematics:
*.lib — containing the symbol description, pins and graphical information
i.e. shape, size or filled color.*.dcm — these the library documentations for
each symbol


For PCB Layouts:
*.pretty — Footprint library folders. The folder itself is the
library.*.kicad_mod — Footprint files, containing one footprint description each.


To configure the Libraries, you can go to Manage Symbol Libraries under
Preferences and add the following path in the Project Specific Libraries.
Replace the symbol_name.lib with the filename of the symbol you want to use in
your project.

${KIPRJMOD}/Libraries/symbol_name.lib


You can go to Manage Footprint Libraries also under Preferences and add the
following path in the Project Specific Libraries. Just replace the
footprint.pretty with the filename of the footprint library you want to use in
your project.

${KIPRJMOD}/Libraries/symbol_name.lib
