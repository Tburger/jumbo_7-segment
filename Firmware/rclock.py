#!/usr/bin/python
# -*- coding: utf-8 -*-

from timeloop import Timeloop
from datetime import timedelta
from typing import List
from rdigit import *


class RClock:
    display: List[Digit] = []

    def __init__(self):
        for i in range(0, MAX_DIGITS):
            self.display.append(Digit(CLOCK[i]))

    def set_time(self, lt: int) -> None:
        t: List[int] = [0, 0, 0, 0]
        t[1], t[0] = divmod(lt[4], 10)  # Minutes
        t[3], t[2] = divmod(lt[3], 10)  # Hours
        for index, digit in enumerate(self.display):
            digit.set(t[index])

    def print_time(self) -> None:
        print(str(self.display[3]) + str(self.display[2]) + ":" + str(self.display[1]) + str(self.display[0]))


class Dp:
    dp: int = 0

    def toggle(self) -> None:
        self.dp ^= 1
        gpio_output(DP, self.dp)


# https://medium.com/greedygame-engineering/an-elegant-way-to-run-periodic-tasks-in-python-61b7c477b679
tl = Timeloop()
rclock = RClock()
#dp = Dp()


@tl.job(interval=timedelta(seconds=1))
def every_1s() -> None:
    lt = time.localtime(time.time())
    rclock.set_time(lt)


# Blinking DP
# @tl.job(interval=timedelta(seconds=1))
# def dp_every_1s():
#    dp.toggle()


if __name__ == "__main__":
    tl.start(block=True)
