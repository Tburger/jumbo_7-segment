#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import timedelta
from typing import List
from timeloop import Timeloop

from rdigit import *


class RCounter:
    display: List[Digit] = []
    value: int = 0
    overrun_flag: bool = False

    def __init__(self):
        for i in range(0, MAX_DIGITS):
            self.display.append(Digit(CLOCK[i]))

    def start_value(self, value: int) -> None:
        self.value = value

    def display_numbers(self, value: int) -> None:
        numbers = list(str(value))
        numbers.reverse()

        for i, number in enumerate(numbers):
            self.display[i].set(int(number))

    def display_blank(self) -> None:
        for j in range(1, MAX_DIGITS):
            self.display[j].blank()
        self.display[0].set(0)

    def next(self) -> None:
        if self.overrun_flag:
            self.value = 0
            self.display_blank()
            self.overrun_flag = False

        self.display_numbers(self.value)
        self.value += 1

        if self.value == 10 ** MAX_DIGITS:
            self.overrun_flag = True


# https://medium.com/greedygame-engineering/an-elegant-way-to-run-periodic-tasks-in-python-61b7c477b679
tl = Timeloop()
counter = RCounter()


@tl.job(interval=timedelta(seconds=0.25))
def every_1s() -> None:
    counter.next()


if __name__ == "__main__":
    tl.start(block=True)
