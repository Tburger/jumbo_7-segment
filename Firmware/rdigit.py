#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from raspi_setup import *


class Digit:
    value: int = 0
    pin: int = 0

    def __init__(self, pin: int):
        self.pin = pin  # latch enable pin on raspberry pi
        self.blank()

    def set(self, value: int) -> bool:
        # Action take place if value has changed
        if self.value == value:
            return False
        else:
            self.value = value
            self.write()
            return True

    def latch_enable(self) -> None:
        gpio_output(self.pin, 0)  # H-->L edge then ...
        time.sleep(0.001)  # wait 1ms and reset latch enable
        gpio_output(self.pin, 1)  # L-->H edge

    def set_bcd(self) -> None:
        for i in range(0, 4):
            gpio_output(BCD[i], (self.value >> i) & 1)

    def write(self) -> None:
        self.set_bcd()
        self.latch_enable()

    def blank(self) -> None:
        self.value = 10  # if value > 9 display turn to blank
        self.write()

    def __str__(self):
        return str(self.value)
