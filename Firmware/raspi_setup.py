import RPi.GPIO as GPIO

# https://www.reddit.com/r/Python/comments/5eddp5/mock_testing_rpigpio/
# https://raspberrypi.stackexchange.com/questions/67452/how-to-setup-a-python-project-wit-gpios-on-intellij-idea-on-windows/67454
# Use P1 header pin numbering convention
GPIO.setmode(GPIO.BOARD)

# Pin configuration
BCD_A = 7  # bit 0
BCD_B = 11  # bit 1
BCD_C = 13  # bit 2
BCD_D = 15  # bit 3

SINGLE_MINUTES = 21  # _ _ : _ 8
TENNER_MINUTES = 23  # _ _ : 8 _
SINGLE_HOURS = 12  # _ 8 : _ _
TENNER_HOURS = 16  # 8 _ : _ _

DP = 19  # Decimal point
# TODO: TASTER = 10 # Taster input

BCD = [BCD_A, BCD_B, BCD_C, BCD_D]
CLOCK = [SINGLE_MINUTES, TENNER_MINUTES, SINGLE_HOURS, TENNER_HOURS]

# Digits are used
MAX_DIGITS = 4


def gpio_setup(pin: int, port_direction, initial: int = 0):
    GPIO.setup(pin, port_direction, initial=initial)


def gpio_output(pin: int, val: bool):
    GPIO.output(pin, not val)


# Set up the GPIO channels
for i in BCD:
    gpio_setup(i, GPIO.OUT, 0)

for i in CLOCK:
    gpio_setup(i, GPIO.OUT, 0)

gpio_setup(DP, GPIO.OUT)


# TODO: GPIO.setup(TASTER, GPIO.IN)